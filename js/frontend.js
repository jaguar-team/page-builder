(function($) {
    "use strict";
    window.azh = $.extend({}, window.azh);
    $(function() {
        $('.az-tabs').each(function() {
            var tabs = this;
            if (!$(tabs).data('az-tabs')) {
                $(tabs).find('> div:first-child > span > a[href^="#"]').click(function(event) {
                    event.preventDefault();
                    $(this).parent().addClass("current");
                    $(this).parent().siblings().removeClass("current");
                    var tab = $(this).attr("href");
                    $(tabs).find('> div:last-child > div').not(tab).css("display", "none");
                    $(tab).fadeIn();
                });
                $(tabs).find('> div:first-child > span:first-child > a[href^="#"]').click();
                $(tabs).data('az-tabs', true);
            }
        });
        $('.az-accordion').each(function() {
            var accordion = this;
            if (!$(accordion).data('az-accordion')) {
                $(accordion).find('> div > div:first-child').click(function(event) {
                    $(this).parent().addClass("current").find('> div:last-child').slideDown();
                    $(this).parent().siblings().removeClass("current").find('> div:last-child').slideUp();
                });
                $(accordion).find('> div:first-child > div:first-child').click();
                $(accordion).data('az-accordion', true);
            }
        });
        if ('flexslider' in $.fn) {
            $('.az-slider').each(function() {
                var slider = this;
                if (!$(slider).data('az-slider')) {
                    $(slider).find('> div:first-child').flexslider({
                        selector: '> div',
                        smoothHeight: true,
                        prevText: '',
                        nextText: '',
                        touch: true,
                        pauseOnHover: true,
                        mousewheel: false,
                        controlNav: false,
                        customDirectionNav: $(slider).find('> div:last-child a')
                    });
                    $(slider).data('az-slider', true);
                }
            });
        }
        if ('owlCarousel' in $.fn) {
            $('.az-carousel').each(function() {
                var carousel = this;
                if (!$(carousel).data('az-carousel')) {
                    $(carousel).owlCarousel({
                        responsive: $(carousel).data('responsive'),
                        center: ($(carousel).data('center') == 'yes'),
                        margin: $(carousel).data('margin'),
                        loop: ($(carousel).data('loop') == 'yes'),
                        autoplay: ($(carousel).data('autoplay') == 'yes'),
                        autoplayHoverPause: true,
                        nav: true,
                        navText: ['', '']
                    });
                    $(carousel).data('az-carousel', true);
                }
            });
        }
        if ('magnificPopup' in $.fn) {
            $('.az-gallery').each(function() {
                $(this).magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    gallery: {
                        enabled: true
                    }
                });
            });
            $('a.az-image-popup').magnificPopup({
                type: 'image',
                removalDelay: 300,
                mainClass: 'mfp-fade',
                overflowY: 'scroll'
            });
            $('a.az-iframe-popup').magnificPopup({
                type: 'iframe',
                removalDelay: 300,
                mainClass: 'mfp-fade',
                overflowY: 'scroll',
                iframe: {
                    markup: '<div class="mfp-iframe-scaler">' +
                            '<div class="mfp-close"></div>' +
                            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                            '</div>',
                    patterns: {
                        youtube: {
                            index: 'youtube.com/',
                            id: 'v=',
                            src: '//www.youtube.com/embed/%id%?autoplay=1'
                        },
                        vimeo: {
                            index: 'vimeo.com/',
                            id: '/',
                            src: '//player.vimeo.com/video/%id%?autoplay=1'
                        },
                        gmaps: {
                            index: '//maps.google.',
                            src: '%id%&output=embed'
                        }
                    },
                    srcAction: 'iframe_src'
                }
            });
        }
        if ('countdown' in $.fn) {
            $('.az-countdown').each(function() {
                var countdown = this;
                if ($(countdown).data('countdownInstance') == undefined) {
                    $(countdown).countdown($(countdown).data('time'), function(event) {
                        $(countdown).find('.az-days .az-count').text(event.offset.totalDays);
                        $(countdown).find('.az-hours .az-count').text(event.offset.hours);
                        $(countdown).find('.az-minutes .az-count').text(event.offset.minutes);
                        $(countdown).find('.az-seconds .az-count').text(event.offset.seconds);
                    });
                }
            });
        }
        if ('waypoint' in $.fn) {
            $('.az-lazy-load').each(function() {
                var image = this;
                var waypoint_handler = function(direction) {
                    $('<img src="' + $(image).data('src') + '">').load(function() {
                        if ($(image).prop('tagName') == 'IMG') {
                            $(image).attr('src', $(image).data('src'));
                        } else {
                            $(image).css('background-image', 'url("' + $(image).data('src') + '")');
                        }
                        $(image).addClass('loaded');
                    });
                }
                $(image).waypoint(waypoint_handler, {offset: '100%', triggerOnce: true});
                $(image).data('waypoint_handler', waypoint_handler);
            });
        }
    });
})(jQuery);