<?php

/*
  Plugin Name: AZEXO HTML customizer extension
  Plugin URI: http://azexo.com
  Description: AZEXO HTML customizer extension
  Author: azexo
  Author URI: http://azexo.com
  Version: 1.23
  Text Domain: azh
 */

add_action('admin_init', 'azh_extension_options');

function azh_extension_options() {
    if (file_exists(dirname(__FILE__) . '/azh_settings.json')) {
        $settings = get_option('azh-settings');
        if (!is_array($settings)) {
            azh_filesystem();
            global $wp_filesystem;
            $settings = $wp_filesystem->get_contents(dirname(__FILE__) . '/azh_settings.json');
            update_option('azh-settings', json_decode($settings, true));
        }
    }
    if (class_exists('WPLessPlugin')) {
        add_settings_field(
                'brand-color', // Field ID
                esc_html__('Brand color', 'azh'), // Label to the left
                'azh_textfield', // Name of function that renders options on the page
                'azh-settings', // Page to show on
                'azh_general_options_section', // Associate with which settings section?
                array(
            'id' => 'brand-color',
            'type' => 'color',
            'default' => '#FF0000',
                )
        );
        add_settings_field(
                'accent-1-color', // Field ID
                esc_html__('Accent 1 color', 'azh'), // Label to the left
                'azh_textfield', // Name of function that renders options on the page
                'azh-settings', // Page to show on
                'azh_general_options_section', // Associate with which settings section?
                array(
            'id' => 'accent-1-color',
            'type' => 'color',
            'default' => '#00FF00',
                )
        );
        add_settings_field(
                'accent-2-color', // Field ID
                esc_html__('Accent 2 color', 'azh'), // Label to the left
                'azh_textfield', // Name of function that renders options on the page
                'azh-settings', // Page to show on
                'azh_general_options_section', // Associate with which settings section?
                array(
            'id' => 'accent-2-color',
            'type' => 'color',
            'default' => '#0000FF',
                )
        );

        add_settings_field(
                'google-fonts', // Field ID
                esc_html__('Google fonts families', 'azh'), // Label to the left
                'azh_textarea', // Name of function that renders options on the page
                'azh-settings', // Page to show on
                'azh_general_options_section', // Associate with which settings section?
                array(
            'id' => 'google-fonts',
            'default' => "Open+Sans:300,400,500,600,700\n"
            . "Montserrat:400,700\n"
            . "Droid+Serif:400,700",
                )
        );
    }

    add_settings_field(
            'css-provided', // Field ID
            esc_html__('CSS provided', 'azh'), // Label to the left
            'azh_checkbox', // Name of function that renders options on the page
            'azh-settings', // Page to show on
            'azh_general_options_section', // Associate with which settings section?
            array(
        'id' => 'css-provided',
        'default' => array(
            'bootstrap-grid' => '0',
            'bootstrap-container' => '0',
        ),
        'options' => array(
            'bootstrap-grid' => esc_html__('Bootstrap row/column classes', 'azh'),
            'bootstrap-container' => esc_html__('Bootstrap container class', 'azh'),
        ),
            )
    );

    add_settings_field(
            'prefix', // Field ID
            esc_html__('Prefix', 'azh'), // Label to the left
            'azh_textfield', // Name of function that renders options on the page
            'azh-settings', // Page to show on
            'azh_general_options_section', // Associate with which settings section?
            array(
        'id' => 'prefix',
        'default' => "azh",
            )
    );

    add_settings_field(
            'container-widths', // Field ID
            esc_html__('Container class widths', 'azh'), // Label to the left
            'azh_textarea', // Name of function that renders options on the page
            'azh-settings', // Page to show on
            'azh_general_options_section', // Associate with which settings section?
            array(
        'id' => 'container-widths',
        'default' => "768px:750px\n"
        . "992px:970px\n"
        . "1200px:1170px",
            )
    );
}

add_filter('wp-less_stylesheet_compute_target_path', 'azh_extension_target_path');

function azh_extension_target_path($target_path) {
    $target_path = preg_replace('#^' . plugin_dir_url('') . '#U', '/', $target_path);
    return $target_path;
}

add_action('wp_enqueue_scripts', 'azh_extension_styles', 1000);

function azh_extension_styles() {
    $settings = get_option('azh-settings');
    if (isset($settings['google-fonts'])) {
        $families = explode("\n", $settings['google-fonts']);
        if (is_array($families)) {
            $font_families = array();
            foreach ($families as $font_family) {
                $font = explode(':', $font_family);
                if (!empty($font_family)) {
                    if ('off' !== esc_html_x('on', $font[0] . ' font: on or off', 'azh')) {
                        $font_families[] = $font_family;
                    }
                }
            }
            if (!empty($font_families)) {
                $query_args = array(
                    'family' => implode(urlencode('|'), $font_families),
                    'subset' => 'latin,latin-ext',
                );
                $fonts_url = add_query_arg($query_args, (is_ssl() ? 'https' : 'http') . '://fonts.googleapis.com/css');
                wp_enqueue_style('azh-extension-fonts', $fonts_url, array(), null);
            }
        }
    }

    if (file_exists(untrailingslashit(dirname(__FILE__)) . '/css/skin.css')) {
        $skin_style = plugins_url('', __FILE__) . '/css/skin.css';
    }

    if (class_exists('WPLessPlugin')) {
        $less = WPLessPlugin::getInstance();
        if (isset($settings['brand-color'])) {
            $less->addVariable('brand-color', $settings['brand-color']);
        }
        if (isset($settings['accent-1-color'])) {
            $less->addVariable('accent-1-color', $settings['accent-1-color']);
        }
        if (isset($settings['accent-2-color'])) {
            $less->addVariable('accent-2-color', $settings['accent-2-color']);
        }
        if (isset($settings['google-fonts'])) {
            $families = explode("\n", $settings['google-fonts']);
            if (is_array($families)) {
                $i = 0;
                foreach ($families as $font_family) {
                    $font = explode(':', $font_family);
                    $i++;
                    $less->addVariable('google-font-family-' . $i, str_replace('+', ' ', $font[0]));
                }
            }
        }
        $less->dispatch();
        if (file_exists(untrailingslashit(dirname(__FILE__)) . '/less/skin.less')) {
            $skin_style = plugins_url('', __FILE__) . '/less/skin.less';
        }
    }
    if (!empty($skin_style)) {
        wp_enqueue_style('azexo-extension-skin', $skin_style);
    }
    if (isset($settings['css-provided'])) {
        if ($settings['css-provided']['bootstrap-grid'] != '0') {
            wp_enqueue_style('azexo-extension-grid', plugins_url('', __FILE__) . '/css/grid.css');
        }
        if ($settings['css-provided']['bootstrap-container'] != '0') {
            if (isset($settings['container-widths'])) {
                $widths = explode("\n", trim($settings['container-widths']));
            }
            $custom_css = "." . $settings['prefix'] . " .container {
                margin-left: auto;
                margin-right: auto;
                box-sizing: border-box;
            }";
            for ($i = 0; $i < count($widths); $i++) {
                $width = $widths[$i];
                $min_max = explode(":", $width);
                if ($i == 0) {
                    $custom_css .= "@media (max-width: " . $min_max[0] . ") {
                        ." . $settings['prefix'] . " .container {
                            padding-right: 15px;
                            padding-left: 15px;
                        }
                    }";
                }
                $custom_css .= "@media (min-width: " . $min_max[0] . ") {
                    ." . $settings['prefix'] . " .container {
                        max-width: " . $min_max[1] . ";
                    }
                }";
            }
            wp_add_inline_style('azexo-extension-skin', $custom_css);
        }
    }


    wp_enqueue_script('flexslider', plugins_url('js/jquery.flexslider-min.js', __FILE__), array('jquery'), false, true);
    wp_enqueue_script('owl.carousel', plugins_url('js/owl.carousel.min.js', __FILE__), array('jquery'), false, true);
    wp_enqueue_script('isotope', plugins_url('js/isotope.pkgd.js', __FILE__), array('jquery'), false, true);
    wp_enqueue_script('waypoints', plugins_url('js/jquery.waypoints.js', __FILE__), array('jquery'), false, true);
    wp_enqueue_script('azh-extension-frontend', plugins_url('js/frontend.js', __FILE__), array('jquery', 'flexslider', 'owl.carousel'), false, true);
}

add_filter('azh_directory', 'azh_extension_directory');

function azh_extension_directory($dir) {
    $dir[untrailingslashit(dirname(__FILE__)) . '/azh'] = plugins_url('', __FILE__) . '/azh';
    return $dir;
}

add_filter('azh_replaces', 'azh_extension_replaces');

function azh_extension_replaces($replaces) {
    return $replaces;
}

add_filter('azh_settings_sanitize_callback', 'azh_extension_settings_sanitize_callback');

function azh_extension_settings_sanitize_callback($input) {
    if (!file_exists(dirname(__FILE__) . '/azh_settings.json')) {
        azh_filesystem();
        global $wp_filesystem;
        $wp_filesystem->put_contents(dirname(__FILE__) . '/azh_settings.json', json_encode($input), FS_CHMOD_FILE);
    }
    return $input;
}
